package ejemplos.josecabrera.com.udacitycourse.events;

/**
 * Created by jose.cabrera on 11/02/2016.
 */
public class SyncFinishEvent {

    private boolean syncFinish;

    public SyncFinishEvent(boolean syncFinish) {
        this.setSyncFinish(syncFinish);

    }

    public boolean isSyncFinish() {
        return syncFinish;
    }

    public void setSyncFinish(boolean syncFinish) {
        this.syncFinish = syncFinish;
    }
}
