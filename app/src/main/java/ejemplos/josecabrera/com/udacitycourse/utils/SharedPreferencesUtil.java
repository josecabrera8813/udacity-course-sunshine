package ejemplos.josecabrera.com.udacitycourse.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ejemplos.josecabrera.com.udacitycourse.R;

public final class SharedPreferencesUtil {

    public static String getPreferredLocation(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_location_key),
                context.getString(R.string.pref_location_default_value));
    }

    public static String getPreferredMetricSystem(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_metric_system_key),
                context.getString(R.string.pref_metric_system_value_metric));
    }

    public static boolean isMetric(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_metric_system_key),
                context.getString(R.string.pref_metric_system_value_metric))
                .equals(context.getString(R.string.pref_metric_system_value_metric));
    }
}
