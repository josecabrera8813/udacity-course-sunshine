package ejemplos.josecabrera.com.udacitycourse.providers.handlers.delete;

import android.content.UriMatcher;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.providers.WeatherProvider;


public class WeatherDeleteHandler extends DeleteHandler {
    public WeatherDeleteHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        super(weatherDbHelper, uriMatcher);
    }

    @Override
    public int handleRequestDelete(Uri uri, String selection, String[] selectionArgs) {
        if (mUriMatcher.match(uri) == WeatherProvider.WEATHER) {
            int rowsDeleted = mWeatherDbHelper.getWritableDatabase().delete(WeatherContract.TABLE_NAME, selection, selectionArgs);
            return rowsDeleted;
        } else {
            return mSuccessor.handleRequestDelete(uri, selection, selectionArgs);
        }
    }
}
