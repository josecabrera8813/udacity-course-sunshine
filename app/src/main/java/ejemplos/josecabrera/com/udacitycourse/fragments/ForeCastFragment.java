package ejemplos.josecabrera.com.udacitycourse.fragments;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import ejemplos.josecabrera.com.udacitycourse.R;
import ejemplos.josecabrera.com.udacitycourse.SunshineApplication;
import ejemplos.josecabrera.com.udacitycourse.adapters.ForeCastAdapter;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.events.ForecastSelectedEvent;
import ejemplos.josecabrera.com.udacitycourse.events.SyncFinishEvent;
import ejemplos.josecabrera.com.udacitycourse.syncadapters.ForecastSyncAdapter;
import ejemplos.josecabrera.com.udacitycourse.utils.SharedPreferencesUtil;

public class ForeCastFragment extends Fragment {

    private static final String[] FORECAST_COLUMNS = {
            WeatherContract.TABLE_NAME + "." + WeatherContract._ID,
            WeatherContract.COLUMN_DATE,
            WeatherContract.COLUMN_SHORT_DESC,
            WeatherContract.COLUMN_MAX_TEMP,
            WeatherContract.COLUMN_MIN_TEMP,
            LocationContract.COLUMN_LOCATION_SETTING,
            WeatherContract.COLUMN_WEATHER_ID,
            LocationContract.COLUMN_COORD_LAT,
            LocationContract.COLUMN_COORD_LONG
    };

    private static final int FORECAST_LOADER = 0;
    private static final String KEY_POSITION_LIST_VIEW = "KEY_POSITION_LIST_VIEW";

    private ForeCastAdapter mForecastAdapter;
    private ListView mForeCastListView;
    private int positionListView = -1;
    private boolean mTwoPane;

    private LoaderManager.LoaderCallbacks<Cursor> weatherLoader = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            String sortOrder = WeatherContract.COLUMN_DATE + " ASC";
            String locationSetting = SharedPreferencesUtil.getPreferredLocation(getActivity());
            Uri weatherForLocationUri = WeatherContract.buildWeatherLocationWithStartDate(
                    locationSetting, System.currentTimeMillis());

            return new CursorLoader(getActivity(),
                    weatherForLocationUri,
                    FORECAST_COLUMNS,
                    null,
                    null,
                    sortOrder);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mForecastAdapter.swapCursor(data);

            if(mTwoPane) {
                positionListView = 0;
                mForeCastListView.setItemChecked(positionListView, true);
                mForeCastListView.smoothScrollToPosition(positionListView);
                SunshineApplication.getBus().post(new ForecastSelectedEvent(getUriForecastDayDetail(data)));
                return;
            }

            if(positionListView>=0){
                mForeCastListView.setItemChecked(positionListView, true);
                mForeCastListView.smoothScrollToPosition(positionListView);
                return;
            }



        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mForecastAdapter.swapCursor(null);

        }
    };




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fore_cast_fragment, container, false);
        setHasOptionsMenu(true);
        initView(view);
        initListeners();
        if (savedInstanceState != null) {
            restoreState(savedInstanceState);
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(FORECAST_LOADER, null, weatherLoader);
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        SunshineApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SunshineApplication.getBus().unregister(this);
    }

    public void changeSettings() {
        updateWeather();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fore_cast_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                updateWeather();
                return true;
            default:
                break;
        }

        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_POSITION_LIST_VIEW, positionListView);

    }

    public void setUseTodayLayout(boolean twoPane) {
        mTwoPane = twoPane;
        if (mForecastAdapter != null) {
            mForecastAdapter.setTwoPane(twoPane);
        }
    }


    private void restoreState(Bundle savedInstanceState) {
        positionListView = savedInstanceState.getInt(KEY_POSITION_LIST_VIEW);
    }


    private void initListeners() {

        mForeCastListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                if (cursor != null) {
                    positionListView = position;
                    SunshineApplication.getBus().post(new ForecastSelectedEvent(getUriForecastDayDetail(cursor)));
                }
            }
        });
    }

    private Uri getUriForecastDayDetail(Cursor cursor) {
        String locationSetting = SharedPreferencesUtil.getPreferredLocation(getActivity());
        return WeatherContract.buildWeatherLocationWithDate(
                locationSetting, cursor.getLong(cursor.getColumnIndex(WeatherContract.COLUMN_DATE)));
    }


    private void initView(View view) {
        mForeCastListView = (ListView) view.findViewById(R.id.list_view_forecast);
        mForeCastListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        mForecastAdapter = new ForeCastAdapter(getActivity(), null, 0);
        mForeCastListView.setAdapter(mForecastAdapter);

    }

    public void onEvent(SyncFinishEvent event){
        if(event.isSyncFinish()){
            restarLoader();
        }
    }



    private void updateWeather() {
        ForecastSyncAdapter.syncImmediately(getActivity());
    }
    private void restarLoader() {
        getLoaderManager().restartLoader(FORECAST_LOADER, null, weatherLoader);

    }
}
