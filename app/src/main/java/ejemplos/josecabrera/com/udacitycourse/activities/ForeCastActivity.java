package ejemplos.josecabrera.com.udacitycourse.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.View;

import ejemplos.josecabrera.com.udacitycourse.R;
import ejemplos.josecabrera.com.udacitycourse.SunshineApplication;
import ejemplos.josecabrera.com.udacitycourse.events.ForecastSelectedEvent;
import ejemplos.josecabrera.com.udacitycourse.fragments.DetailFragment;
import ejemplos.josecabrera.com.udacitycourse.fragments.ForeCastFragment;
import ejemplos.josecabrera.com.udacitycourse.syncadapters.ForecastSyncAdapter;
import ejemplos.josecabrera.com.udacitycourse.utils.SharedPreferencesUtil;


public class ForeCastActivity extends BaseActivity {

    public static final String KEY_LOCATION = "KEY_LOCATION";
    public static final String KEY_METRIC_SYSTEM = "KEY_METRIC_SYSTEM";
    private boolean mTwoPane = false;
    private String mLocation;
    private String mMetricSystem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ForecastSyncAdapter.initializeSyncAdapter(this);
        setContentView(R.layout.activity_forecast);
        getSupportActionBar().setElevation(0f);

        if (savedInstanceState == null) {
            mLocation = SharedPreferencesUtil.getPreferredLocation(this);
            mMetricSystem = SharedPreferencesUtil.getPreferredMetricSystem(this);
        } else {
            initState(savedInstanceState);
        }

        if (findViewById(R.id.fragment_detail) != null) {
            mTwoPane = true;
            if (savedInstanceState == null) {
                setFragment(getFragmentDetail(), DetailFragment.TAG_FRAGMENT_DETAIL, R.id.fragment_detail);
            }
        } else {
            mTwoPane = false;
        }

        ForeCastFragment forecastFragment =  ((ForeCastFragment)getSupportFragmentManager()
                .findFragmentById(R.id.fragment_forecast));
        forecastFragment.setUseTodayLayout(mTwoPane);


    }



    @Override
    protected void onResume() {
        super.onResume();
        SunshineApplication.getBus().register(this);
        String location = SharedPreferencesUtil.getPreferredLocation(this);
        String metricSystem = SharedPreferencesUtil.getPreferredMetricSystem(this);

        if (!location.equals(mLocation) || !metricSystem.equals(mMetricSystem)) {
            ForeCastFragment forecastFragment = (ForeCastFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_forecast);
            mLocation = location;
            mMetricSystem = metricSystem;
            forecastFragment.changeSettings();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_LOCATION, mLocation);
        outState.putString(KEY_METRIC_SYSTEM, mMetricSystem);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SunshineApplication.getBus().unregister(this);
    }

    @Override
    protected void readExtras() {

    }


    public void onEvent(ForecastSelectedEvent event) {
        if (mTwoPane) {
            Bundle args = new Bundle();
            args.putParcelable(DetailFragment.KEY_DETAIL_URI, event.getUri());
            DetailFragment detailFragment = ((DetailFragment) getSupportFragmentManager()
                    .findFragmentByTag(DetailFragment.TAG_FRAGMENT_DETAIL));
            detailFragment.restartLoader(event.getUri());
        } else {
            Intent intent =
                    new Intent(this, DetailActivity.class).setData(event.getUri());
            startActivity(intent);
        }
    }


    public Fragment getFragmentDetail() {
        return DetailFragment.newInstance(null);
    }

    private void initState(Bundle savedInstanceState) {
        mLocation = savedInstanceState.getString(KEY_LOCATION);
        mMetricSystem = savedInstanceState.getString(KEY_METRIC_SYSTEM);
    }


}
