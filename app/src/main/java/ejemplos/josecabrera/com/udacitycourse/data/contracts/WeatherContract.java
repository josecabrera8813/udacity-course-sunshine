package ejemplos.josecabrera.com.udacitycourse.data.contracts;


import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import ejemplos.josecabrera.com.udacitycourse.utils.DateUtils;

public class WeatherContract extends BaseContract implements BaseColumns {

    public static final String PATH_WEATHER = "weather";

    public static final String CONTENT_TYPE =
            ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_WEATHER;

    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(PATH_WEATHER).build();


    public static final String TABLE_NAME = "weather";
    public static final String COLUMN_LOC_KEY = "location_id";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_WEATHER_ID = "weather_id";
    public static final String COLUMN_SHORT_DESC = "short_desc";
    public static final String COLUMN_MIN_TEMP = "min";
    public static final String COLUMN_MAX_TEMP = "max";
    public static final String COLUMN_HUMIDITY = "humidity";
    public static final String COLUMN_PRESSURE = "pressure";
    public static final String COLUMN_WIND_SPEED = "wind";
    public static final String COLUMN_DEGREES = "degrees";

    public static String getCreateTableSentence() {
        return  "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_LOC_KEY + " INTEGER NOT NULL, " +
                COLUMN_DATE + " INTEGER NOT NULL, " +
                COLUMN_SHORT_DESC + " TEXT NOT NULL, " +
                COLUMN_WEATHER_ID + " INTEGER NOT NULL," +
                COLUMN_MIN_TEMP + " REAL NOT NULL, " +
                COLUMN_MAX_TEMP + " REAL NOT NULL, " +
                COLUMN_HUMIDITY + " REAL NOT NULL, " +
                COLUMN_PRESSURE + " REAL NOT NULL, " +
                COLUMN_WIND_SPEED + " REAL NOT NULL, " +
                COLUMN_DEGREES + " REAL NOT NULL, " +
                " FOREIGN KEY (" + COLUMN_LOC_KEY + ") REFERENCES " +
                LocationContract.TABLE_NAME + " (" + LocationContract._ID + "), " +
                " UNIQUE (" + COLUMN_DATE + ", " +
                COLUMN_LOC_KEY + ") ON CONFLICT REPLACE);";
    }

    public static String getLocationSettingFromUri(Uri uri) {
        return uri.getPathSegments().get(1);
    }

    public static long getDateFromUri(Uri uri) {
        return Long.parseLong(uri.getPathSegments().get(2));
    }

    public static Uri buildWeatherUri(long id) {
        return ContentUris.withAppendedId(CONTENT_URI, id);
    }


    public static Uri buildWeatherLocation(String locationSetting) {
        return CONTENT_URI.buildUpon().appendPath(locationSetting).build();
    }

    public static Uri buildWeatherLocationWithStartDate(
            String locationSetting, long startDate) {
        long normalizedDate = DateUtils.normalizeDate(startDate);
        return CONTENT_URI.buildUpon().appendPath(locationSetting)
                .appendQueryParameter(COLUMN_DATE, Long.toString(normalizedDate)).build();
    }

    public static Uri buildWeatherLocationWithDate(String locationSetting, long date) {
        return CONTENT_URI.buildUpon().appendPath(locationSetting)
                .appendPath(Long.toString(DateUtils.normalizeDate(date))).build();
    }



}
