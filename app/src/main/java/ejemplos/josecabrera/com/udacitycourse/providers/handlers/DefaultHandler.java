package ejemplos.josecabrera.com.udacitycourse.providers.handlers;

import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers.QueryHandler;


public class DefaultHandler extends QueryHandler {


    public DefaultHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        super(weatherDbHelper, uriMatcher);
    }

    @Override
    public Cursor handleRequestQuery(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    @Override
    protected String buildSelection() {
        return null;
    }
}
