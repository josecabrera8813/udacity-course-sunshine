package ejemplos.josecabrera.com.udacitycourse.providers.handlers.delete;

import android.content.UriMatcher;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.providers.WeatherProvider;

public class LocationDeleteHandler extends DeleteHandler {

    public LocationDeleteHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        super(weatherDbHelper, uriMatcher);
    }

    @Override
    public int handleRequestDelete(Uri uri, String selection, String[] selectionArgs) {
        if (mUriMatcher.match(uri) == WeatherProvider.LOCATION) {
            int rowsUpdated = mWeatherDbHelper.getWritableDatabase().delete(LocationContract.TABLE_NAME, selection, selectionArgs);
            return rowsUpdated;
        } else {
            return mSuccessor.handleRequestDelete(uri, selection, selectionArgs);
        }
    }
}
