package ejemplos.josecabrera.com.udacitycourse.providers.handlers.inserthandlers;


import android.content.ContentValues;
import android.content.UriMatcher;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.providers.WeatherProvider;

public class WeatherInsertHandler extends InsertHandler {

    public WeatherInsertHandler( WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        super( weatherDbHelper, uriMatcher);
    }

    @Override
    public Uri handleRequestInsert(Uri uri, ContentValues values) {
        if (mUriMatcher.match(uri) == WeatherProvider.WEATHER) {
            long id = mWeatherDbHelper.getWritableDatabase().insert(WeatherContract.TABLE_NAME, null, values);
            if (id > 0) {
                return WeatherContract.buildWeatherUri(id);
            } else {
                throw new android.database.SQLException("Failed to insert row into " + uri);
            }

        }else {
            return mSuccessor.handleRequestInsert(uri, values);
        }

    }
}
