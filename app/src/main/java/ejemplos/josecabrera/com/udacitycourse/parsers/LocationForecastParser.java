package ejemplos.josecabrera.com.udacitycourse.parsers;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ejemplos.josecabrera.com.udacitycourse.model.LocationForecast;

public class LocationForecastParser {

    private static final String OWM_LIST = "list";
    private static final String OWM_CITY = "city";
    private static final String OWM_CITY_NAME = "name";
    private static final String OWM_COORD = "coord";
    private static final String OWM_LATITUDE = "lat";
    private static final String OWM_LONGITUDE = "lon";

    public static LocationForecast getLocationForecastFromJson(String locationForecastStr) throws JSONException {

        LocationForecast locationForecast = new LocationForecast();
        JSONObject forecastJson = new JSONObject(locationForecastStr);
        JSONArray forecastPerDay = forecastJson.getJSONArray(OWM_LIST);

        locationForecast.setForecast(ForecastDayParser.getForecastListFromJson(forecastPerDay));


        JSONObject cityJson = forecastJson.getJSONObject(OWM_CITY);
        JSONObject cityCoordinates = cityJson.getJSONObject(OWM_COORD);

        locationForecast.setCityName(cityJson.getString(OWM_CITY_NAME));
        locationForecast.setCityLatitude(cityCoordinates.getDouble(OWM_LATITUDE));
        locationForecast.setCityLongitude(cityCoordinates.getDouble(OWM_LONGITUDE));

        return locationForecast;

    }
}
