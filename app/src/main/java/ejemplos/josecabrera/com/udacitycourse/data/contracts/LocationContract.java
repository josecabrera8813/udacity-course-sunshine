package ejemplos.josecabrera.com.udacitycourse.data.contracts;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;


public class LocationContract extends BaseContract implements BaseColumns {

    public static final String PATH_LOCATION = "location";
    public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_LOCATION).build();

    public static final String CONTENT_TYPE =
            ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;


    public static final String TABLE_NAME = "location";
    public static final String COLUMN_LOCATION_SETTING = "location_setting";
    public static final String COLUMN_CITY_NAME = "city_name";
    public static final String COLUMN_COORD_LAT = "coord_lat";
    public static final String COLUMN_COORD_LONG = "coord_long";

    public static String getCreateTableSentence() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_LOCATION_SETTING + " TEXT UNIQUE NOT NULL, " +
                COLUMN_CITY_NAME + " TEXT NOT NULL, " +
                COLUMN_COORD_LAT + " REAL NOT NULL, " +
                COLUMN_COORD_LONG + " REAL NOT NULL " +
                " );";
    }

    public static Uri buildLocationUri(long id) {
        return ContentUris.withAppendedId(CONTENT_URI, id);
    }
}
