package ejemplos.josecabrera.com.udacitycourse.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ejemplos.josecabrera.com.udacitycourse.R;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.utils.DrawableHelper;
import ejemplos.josecabrera.com.udacitycourse.utils.FormatHelper;
import ejemplos.josecabrera.com.udacitycourse.utils.SharedPreferencesUtil;


public class DetailFragment extends Fragment {
    public static final String TAG_FRAGMENT_DETAIL = "TAG_FRAGMENT_DETAIL";
    public static final String KEY_DETAIL_URI = "KEY_DETAIL_URI";
    private static final int DETAIL_WEATHER_LOADER = 1;
    private ShareActionProvider mShareActionProvider;
    private Uri mDetailWeather;

    private LoaderManager.LoaderCallbacks<Cursor> weatherLoader = new LoaderManager.LoaderCallbacks<Cursor>() {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            if(mDetailWeather == null){return null;}
            return new CursorLoader(getActivity(),
                    mDetailWeather,
                    null,
                    null,
                    null,
                    null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

            if (data != null && data.moveToFirst()) {

                long date = data.getLong(data.getColumnIndex(WeatherContract.COLUMN_DATE));
                String friendlyDateText = FormatHelper.getDayName(getActivity(), date);
                String dateText = FormatHelper.getFormattedMonthDay(getActivity(), date);

                String weatherDescription =
                        data.getString(data.getColumnIndex(WeatherContract.COLUMN_SHORT_DESC));

                boolean isMetric = SharedPreferencesUtil.isMetric(getActivity());

                String high = FormatHelper.formatTemperature(getActivity(),
                        data.getDouble(data.getColumnIndex(WeatherContract.COLUMN_MAX_TEMP)), isMetric);

                String low = FormatHelper.formatTemperature(getActivity(),
                        data.getDouble(data.getColumnIndex(WeatherContract.COLUMN_MIN_TEMP)), isMetric);

                Float humidity = data.getFloat(data.getColumnIndex(WeatherContract.COLUMN_HUMIDITY));


                float windSpeedStr = data.getFloat(data.getColumnIndex(WeatherContract.COLUMN_WIND_SPEED));
                float windDirStr = data.getFloat(data.getColumnIndex(WeatherContract.COLUMN_DEGREES));

                Float pressure = data.getFloat(data.getColumnIndex(WeatherContract.COLUMN_PRESSURE));

                ImageView iconView = (ImageView) getView().findViewById(R.id.detail_icon);
                iconView.setImageResource(DrawableHelper
                        .getArtResourceForWeatherCondition(
                                data.getInt(data.getColumnIndex(WeatherContract.COLUMN_WEATHER_ID))));

                TextView dateView = (TextView) getView().findViewById(R.id.detail_date_textview);
                dateView.setText(dateText);

                TextView friendlyDateView = (TextView) getView().findViewById(R.id.detail_day_textview);
                friendlyDateView.setText(friendlyDateText);


                TextView descriptionView = (TextView) getView().findViewById(R.id.detail_forecast_textview);
                descriptionView.setText(weatherDescription);
                TextView highTempView = (TextView) getView().findViewById(R.id.detail_high_textview);
                highTempView.setText(high);
                TextView lowTempView = (TextView) getView().findViewById(R.id.detail_low_textview);
                lowTempView.setText(low);

                TextView humidityTextView = (TextView) getView().findViewById(R.id.detail_humidity_textview);
                humidityTextView.setText(getActivity().getString(R.string.format_humidity, humidity));
                TextView windTextview = (TextView) getView().findViewById(R.id.detail_wind_textview);
                windTextview.setText(FormatHelper.getFormattedWind(getActivity(), windSpeedStr, windDirStr));
                TextView pressureTextview = (TextView) getView().findViewById(R.id.detail_pressure_textview);
                pressureTextview.setText(getActivity().getString(R.string.format_pressure, pressure));


                // If onCreateOptionsMenu has already happened, we need to update the share intent now.
                if (mShareActionProvider != null) {
                    mShareActionProvider.setShareIntent(createIntent());
                }
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(DETAIL_WEATHER_LOADER, null, weatherLoader);
        super.onActivityCreated(savedInstanceState);
    }



    public static DetailFragment newInstance(Uri detail) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_DETAIL_URI, detail);
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.detail_fragment, container, false);
        setHasOptionsMenu(true);
        readArgs();
        initView(view);
        return view;
    }


    private void initView(View view) {

    }

    private void readArgs() {
        if (getArguments() != null && getArguments().containsKey(KEY_DETAIL_URI)) {
            mDetailWeather = getArguments().getParcelable(KEY_DETAIL_URI);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detail_fragment_menu, menu);
        MenuItem item = menu.findItem(R.id.action_share);
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        setShareIntent(createIntent());
        super.onCreateOptionsMenu(menu, inflater);
    }

    private Intent createIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "hola mundo");
        return intent;
    }


    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    public void restartLoader(Uri uri){
        mDetailWeather = uri;
        getLoaderManager().destroyLoader(DETAIL_WEATHER_LOADER);
        getLoaderManager().initLoader(DETAIL_WEATHER_LOADER, null, weatherLoader);
    }

}
