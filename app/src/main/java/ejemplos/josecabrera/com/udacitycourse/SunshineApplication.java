package ejemplos.josecabrera.com.udacitycourse;


import android.app.Application;

import de.greenrobot.event.EventBus;

public class SunshineApplication extends Application{

    private static EventBus bus;

    @Override
    public void onCreate() {
        super.onCreate();
        bus = EventBus.getDefault();
    }

    public static EventBus getBus(){
        return bus;
    }
}
