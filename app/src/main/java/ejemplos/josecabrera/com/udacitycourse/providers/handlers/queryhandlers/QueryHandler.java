package ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers;


import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;

public abstract class QueryHandler {

    protected QueryHandler mSuccessor;
    protected WeatherDbHelper mWeatherDbHelper;
    protected UriMatcher mUriMatcher;

    public QueryHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher)
    {
        mWeatherDbHelper = weatherDbHelper;
        mUriMatcher = uriMatcher;
    }

    public void setSuccessor(QueryHandler successor){
        mSuccessor = successor;

    }

    public abstract Cursor handleRequestQuery(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder);

    protected abstract String buildSelection();
}

