package ejemplos.josecabrera.com.udacitycourse.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import ejemplos.josecabrera.com.udacitycourse.R;


public abstract class BaseActivity extends AppCompatActivity
{

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
        //
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                loadSettingsActivity();
                return true;
            case R.id.action_map:
                loadMap();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadMap() {
        String location = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.pref_location_key), "");
        Uri gmmIntentUri = Uri.parse("geo:0,0?").buildUpon().appendQueryParameter("q", location).build();
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    private void loadSettingsActivity() {
        Intent intent = new Intent(this, SettingsForecastActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        readExtras();
    }


    protected void setFragment(Fragment fragment, String fragmentTag) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragmentInMemory = fm.findFragmentByTag(fragmentTag);
        if (fragmentInMemory != null) {
            ft.replace(R.id.general_fragment, fragmentInMemory, fragmentTag);
            ft.commit();
        } else {
            ft.replace(R.id.general_fragment, fragment, fragmentTag);
            ft.commit();
        }
    }

    protected void setFragment(Fragment fragment, String fragmentTag, int conteiner) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragmentInMemory = fm.findFragmentByTag(fragmentTag);
        if (fragmentInMemory != null) {
            ft.replace(conteiner, fragmentInMemory, fragmentTag);
        } else {
            ft.replace(conteiner, fragment, fragmentTag);
        }
        ft.commit();

    }

    protected abstract void readExtras();
}
