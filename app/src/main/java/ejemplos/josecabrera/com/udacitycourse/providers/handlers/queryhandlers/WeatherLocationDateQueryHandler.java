package ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers;

import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.providers.WeatherProvider;

/**
 * Created by jose.cabrera on 07/01/2016.
 */
public class WeatherLocationDateQueryHandler extends QueryHandler {
    private final SQLiteQueryBuilder mWeatherByLocationSettingQueryBuilder;

    public WeatherLocationDateQueryHandler( WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        super(weatherDbHelper,uriMatcher);
        mWeatherByLocationSettingQueryBuilder = new SQLiteQueryBuilder();
        mWeatherByLocationSettingQueryBuilder.setTables(getTables());
    }

    @Override
    public Cursor handleRequestQuery(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        if(mUriMatcher.match(uri)== WeatherProvider.WEATHER_WITH_LOCATION_AND_DATE){
            String locationSetting = WeatherContract.getLocationSettingFromUri(uri);
            long date = WeatherContract.getDateFromUri(uri);

            Cursor cursor = mWeatherByLocationSettingQueryBuilder.query(
                    mWeatherDbHelper.getReadableDatabase(),
                    projection,
                    buildSelection(),
                    new String[]{locationSetting, Long.toString(date)},
                    null,
                    null,
                    sortOrder
            );
            return cursor;
        }else {
            return mSuccessor.handleRequestQuery(uri, projection, selection, selectionArgs, sortOrder);
        }

    }

    @Override
    public String buildSelection() {
        return LocationContract.TABLE_NAME +
                "." + LocationContract.COLUMN_LOCATION_SETTING + " = ? AND " +
                WeatherContract.COLUMN_DATE + " = ? ";
    }

    public String getTables() {
        return  WeatherContract.TABLE_NAME + " INNER JOIN " +
                LocationContract.TABLE_NAME +
                " ON " + WeatherContract.TABLE_NAME +
                "." + WeatherContract.COLUMN_LOC_KEY +
                " = " + LocationContract.TABLE_NAME +
                "." + LocationContract._ID;
    }
}
