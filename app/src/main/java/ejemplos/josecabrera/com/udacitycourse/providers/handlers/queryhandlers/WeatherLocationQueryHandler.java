package ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers;

import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.providers.WeatherProvider;


public class WeatherLocationQueryHandler extends QueryHandler {

    private final SQLiteQueryBuilder mWeatherByLocationSettingQueryBuilder;

    public WeatherLocationQueryHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        super(weatherDbHelper, uriMatcher);
        mWeatherByLocationSettingQueryBuilder = new SQLiteQueryBuilder();
        mWeatherByLocationSettingQueryBuilder.setTables(getTables());
    }

    @Override
    public Cursor handleRequestQuery(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        if (mUriMatcher.match(uri) == WeatherProvider.WEATHER_WITH_LOCATION) {
            String locationSetting = WeatherContract.getLocationSettingFromUri(uri);

            return mWeatherByLocationSettingQueryBuilder.query(
                    mWeatherDbHelper.getReadableDatabase(),
                    projection,
                    buildSelection(),
                    new String[]{locationSetting},
                    null,
                    null,
                    sortOrder
            );
        } else {
            return mSuccessor.handleRequestQuery(uri, projection, selection, selectionArgs, sortOrder);
        }

    }

    @Override
    public String buildSelection() {
        return LocationContract.TABLE_NAME +
                "." + LocationContract.COLUMN_LOCATION_SETTING + " = ?";
    }

    public String getTables() {
        return WeatherContract.TABLE_NAME + " INNER JOIN " +
                LocationContract.TABLE_NAME +
                " ON " + WeatherContract.TABLE_NAME +
                "." + WeatherContract.COLUMN_LOC_KEY +
                " = " + LocationContract.TABLE_NAME +
                "." + LocationContract._ID;
    }
}
