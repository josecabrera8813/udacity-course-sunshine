package ejemplos.josecabrera.com.udacitycourse.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;

public class WeatherDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "weather.db";
    private static WeatherDbHelper mWeatherDbHelper = null;

    public static WeatherDbHelper newInstance(Context context){
        if(mWeatherDbHelper == null){
            mWeatherDbHelper = new WeatherDbHelper(context);
            return mWeatherDbHelper;
        }
        return mWeatherDbHelper;
    }

    private WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(LocationContract.getCreateTableSentence());
        sqLiteDatabase.execSQL(WeatherContract.getCreateTableSentence());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
