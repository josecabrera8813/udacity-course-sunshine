package ejemplos.josecabrera.com.udacitycourse.providers.handlers.delete;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;


public abstract  class DeleteHandler {

    protected DeleteHandler mSuccessor;
    protected WeatherDbHelper mWeatherDbHelper;
    protected UriMatcher mUriMatcher;

    public DeleteHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher){
        mWeatherDbHelper = weatherDbHelper;
        mUriMatcher = uriMatcher;
    }

    public void setSuccessor(DeleteHandler successor){
        mSuccessor = successor;
    }

    public abstract int handleRequestDelete (Uri uri, String selection, String[] selectionArgs);
}
