package ejemplos.josecabrera.com.udacitycourse.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import ejemplos.josecabrera.com.udacitycourse.R;
import ejemplos.josecabrera.com.udacitycourse.fragments.DetailFragment;


public class DetailActivity extends BaseActivity {


    private static final String TAG_FRAGMENT_DETAIL = "TAG_FRAGMENT_DETAIL";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_single_fragment);
        setFragment(getFragmentDetail(), TAG_FRAGMENT_DETAIL);
    }

    @Override
    protected void readExtras() {

    }


    public Fragment getFragmentDetail() {
        return DetailFragment.newInstance(getIntent().getData());
    }
}
