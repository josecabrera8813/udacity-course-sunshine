package ejemplos.josecabrera.com.udacitycourse.providers.handlers.updatehandlers;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.providers.WeatherProvider;


public class LocationUpdateHandler extends UpdateHandler {

    public LocationUpdateHandler(UpdateHandler successor, WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        super(successor, weatherDbHelper, uriMatcher);
    }

    @Override
    public int handleRequestUpdate(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (mUriMatcher.match(uri) == WeatherProvider.LOCATION) {
            int rowsUpdated = mWeatherDbHelper.getWritableDatabase().update(LocationContract.TABLE_NAME, values, selection,
                    selectionArgs);
            return rowsUpdated;
        } else {
            return mSuccessor.handleRequestUpdate(uri, values, selection, selectionArgs);
        }
    }
}
