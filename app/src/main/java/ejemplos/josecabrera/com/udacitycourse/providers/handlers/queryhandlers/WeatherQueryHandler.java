package ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers;


import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.providers.WeatherProvider;

public class WeatherQueryHandler extends QueryHandler {

    public WeatherQueryHandler( WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        super( weatherDbHelper, uriMatcher);
    }


    @Override
    public Cursor handleRequestQuery(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (mUriMatcher.match(uri) == WeatherProvider.WEATHER) {
            return mWeatherDbHelper.getReadableDatabase().query(
                    WeatherContract.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder);

        } else {
            return mSuccessor.handleRequestQuery(uri, projection, selection, selectionArgs, sortOrder);
        }
    }

    @Override
    protected String buildSelection() {
        return null;
    }
}
