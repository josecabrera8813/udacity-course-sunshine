package ejemplos.josecabrera.com.udacitycourse.factories;


import android.content.UriMatcher;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.DefaultHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.delete.DeleteHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.delete.LocationDeleteHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.delete.WeatherDeleteHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.inserthandlers.InsertHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.inserthandlers.LocationInsertHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.inserthandlers.WeatherInsertHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers.LocationQueryHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers.QueryHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers.WeatherLocationDateQueryHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers.WeatherLocationQueryHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers.WeatherQueryHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.updatehandlers.UpdateHandler;

public class HandlersFactory {

    public static QueryHandler createQueryHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        QueryHandler locationQuery = new LocationQueryHandler(weatherDbHelper, uriMatcher);
        QueryHandler weatherLocationDateQuery = new WeatherLocationDateQueryHandler(weatherDbHelper, uriMatcher);
        QueryHandler weatherLocationQuery = new WeatherLocationQueryHandler(weatherDbHelper, uriMatcher);
        QueryHandler weatherQuery = new WeatherQueryHandler(weatherDbHelper, uriMatcher);
        locationQuery.setSuccessor(weatherLocationDateQuery);
        weatherLocationDateQuery.setSuccessor(weatherLocationQuery);
        weatherLocationQuery.setSuccessor(weatherQuery);
        weatherQuery.setSuccessor(new DefaultHandler(weatherDbHelper, uriMatcher));
        return locationQuery;
    }

    public static InsertHandler createInsertHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        InsertHandler locationInsert = new LocationInsertHandler(weatherDbHelper,uriMatcher);
        InsertHandler weatherInsert = new WeatherInsertHandler(weatherDbHelper,uriMatcher);
        locationInsert.setSuccessor(weatherInsert);
        return locationInsert;
    }

    public static DeleteHandler createDeleteHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        DeleteHandler locationDelete = new LocationDeleteHandler(weatherDbHelper,uriMatcher);
        DeleteHandler weatherDelete = new WeatherDeleteHandler(weatherDbHelper,uriMatcher);
        locationDelete.setSuccessor(weatherDelete);
        return locationDelete;

    }

    public static UpdateHandler createUpdateHandler(WeatherDbHelper mWeatherDbHelper, UriMatcher mUriMatcher) {
        return null;
    }
}
