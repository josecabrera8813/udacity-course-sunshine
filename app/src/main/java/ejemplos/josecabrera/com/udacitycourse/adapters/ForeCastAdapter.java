package ejemplos.josecabrera.com.udacitycourse.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ejemplos.josecabrera.com.udacitycourse.R;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.utils.DrawableHelper;
import ejemplos.josecabrera.com.udacitycourse.utils.FormatHelper;
import ejemplos.josecabrera.com.udacitycourse.utils.SharedPreferencesUtil;


public class ForeCastAdapter extends CursorAdapter {

    private static final int VIEW_TYPE_TODAY = 0;
    private static final int VIEW_TYPE_FUTURE_DAY = 1;
    private static final int VIEW_TYPE_COUNT = 2;

    private boolean mTwoPane= false;

    public ForeCastAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        int viewType = getItemViewType(cursor.getPosition());
        View view;
        if (viewType == VIEW_TYPE_TODAY) {
            view = LayoutInflater.from(context).inflate(R.layout.list_item_forecast_today, parent, false);

        } else {
            view = LayoutInflater.from(context).inflate(R.layout.list_item_forecast, parent, false);
        }

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        int viewType = getItemViewType(cursor.getPosition());
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        if (viewType == VIEW_TYPE_TODAY) {
            viewHolder.iconView.setImageResource(DrawableHelper
                    .getArtResourceForWeatherCondition(
                            cursor.getInt(cursor.getColumnIndex(WeatherContract.COLUMN_WEATHER_ID))));
        } else {
            viewHolder.iconView.setImageResource(DrawableHelper
                    .getIconResourceForWeatherCondition(
                            cursor.getInt(cursor.getColumnIndex(WeatherContract.COLUMN_WEATHER_ID))));
        }


        long date = cursor.getLong(cursor.getColumnIndex(WeatherContract.COLUMN_DATE));
        viewHolder.dateView.setText(FormatHelper.getFriendlyDayString(mContext, date));

        String forecast = cursor.getString(cursor.getColumnIndex(WeatherContract.COLUMN_SHORT_DESC));
        viewHolder.descriptionView.setText(forecast);

        boolean isMetric = SharedPreferencesUtil.isMetric(context);


        double high = cursor.getDouble(cursor.getColumnIndex(WeatherContract.COLUMN_MAX_TEMP));
        viewHolder.highTempView.setText(FormatHelper.formatTemperature(mContext, high, isMetric));

        double low = cursor.getDouble(cursor.getColumnIndex(WeatherContract.COLUMN_MIN_TEMP));
        viewHolder.lowTempView.setText(FormatHelper.formatTemperature(mContext, low, isMetric));
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 && !mTwoPane ? VIEW_TYPE_TODAY : VIEW_TYPE_FUTURE_DAY;
    }

    public void setTwoPane(boolean twoPane){
        mTwoPane = twoPane;
    }

    public static class ViewHolder {
        public final ImageView iconView;
        public final TextView dateView;
        public final TextView descriptionView;
        public final TextView highTempView;
        public final TextView lowTempView;

        public ViewHolder(View view) {
            iconView = (ImageView) view.findViewById(R.id.list_item_icon);
            dateView = (TextView) view.findViewById(R.id.list_item_date_textview);
            descriptionView = (TextView) view.findViewById(R.id.list_item_forecast_textview);
            highTempView = (TextView) view.findViewById(R.id.list_item_high_textview);
            lowTempView = (TextView) view.findViewById(R.id.list_item_low_textview);
        }
    }
}