package ejemplos.josecabrera.com.udacitycourse.providers.handlers.updatehandlers;


import android.content.ContentValues;
import android.content.UriMatcher;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;

public abstract class UpdateHandler {
    protected UpdateHandler mSuccessor;
    protected WeatherDbHelper mWeatherDbHelper;
    protected UriMatcher mUriMatcher;

    public UpdateHandler(UpdateHandler successor,WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher){
        mWeatherDbHelper = weatherDbHelper;
        mUriMatcher = uriMatcher;
        mSuccessor = successor;
    }

    public abstract int handleRequestUpdate(Uri uri, ContentValues values, String selection, String[] selectionArgs);
}
