package ejemplos.josecabrera.com.udacitycourse.events;

import android.net.Uri;


public class ForecastSelectedEvent {

    private Uri uri;

    public ForecastSelectedEvent(Uri uriForecastDayDetail) {
        uri = uriForecastDayDetail;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }
}
