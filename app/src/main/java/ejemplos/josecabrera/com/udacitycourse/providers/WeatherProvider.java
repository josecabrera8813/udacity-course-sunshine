package ejemplos.josecabrera.com.udacitycourse.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.factories.HandlersFactory;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.delete.DeleteHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.inserthandlers.InsertHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers.QueryHandler;
import ejemplos.josecabrera.com.udacitycourse.providers.handlers.updatehandlers.UpdateHandler;
import ejemplos.josecabrera.com.udacitycourse.utils.DateUtils;


public class WeatherProvider extends ContentProvider {

    public static final int WEATHER = 100;
    public static final int WEATHER_WITH_LOCATION = 101;
    public static final int WEATHER_WITH_LOCATION_AND_DATE = 102;
    public static final int LOCATION = 200;

    private WeatherDbHelper mWeatherDbHelper;
    private UriMatcher mUriMatcher;

    @Override
    public boolean onCreate() {
        mWeatherDbHelper = WeatherDbHelper.newInstance(getContext());
        mUriMatcher = buildUriMatcher();
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        QueryHandler queryHandler = HandlersFactory.createQueryHandler(mWeatherDbHelper, mUriMatcher);
        return queryHandler.handleRequestQuery(uri, projection, selection, selectionArgs, sortOrder);
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
        InsertHandler insertHandler = HandlersFactory.createInsertHandler(mWeatherDbHelper, mUriMatcher);
        getContext().getContentResolver().notifyChange(uri, null);
        return insertHandler.handleRequestInsert(uri, values);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        DeleteHandler deleteHandler = HandlersFactory.createDeleteHandler(mWeatherDbHelper, mUriMatcher);
        int rowsDeleted = deleteHandler.handleRequestDelete(uri, selection, selectionArgs);
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        UpdateHandler updateHandler = HandlersFactory.createUpdateHandler(mWeatherDbHelper, mUriMatcher);
        return updateHandler.handleRequestUpdate(uri, values, selection, selectionArgs);
    }

    private UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = WeatherContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, WeatherContract.PATH_WEATHER, WEATHER);
        matcher.addURI(authority, WeatherContract.PATH_WEATHER + "/*", WEATHER_WITH_LOCATION);
        matcher.addURI(authority, WeatherContract.PATH_WEATHER + "/*/#", WEATHER_WITH_LOCATION_AND_DATE);
        matcher.addURI(authority, LocationContract.PATH_LOCATION, LOCATION);
        return matcher;
    }

    //TODO Refactor
    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mWeatherDbHelper.getWritableDatabase();
        final int match = mUriMatcher.match(uri);
        switch (match) {
            case WEATHER:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(WeatherContract.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }

    @Override
    public String getType(Uri uri) {
        final int match = mUriMatcher.match(uri);
        switch (match) {
            case WEATHER:
                return WeatherContract.CONTENT_TYPE;
            case LOCATION:
                return LocationContract.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException();
        }
    }


}
