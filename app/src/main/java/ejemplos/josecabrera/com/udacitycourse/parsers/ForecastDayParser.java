package ejemplos.josecabrera.com.udacitycourse.parsers;


import android.text.format.Time;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import ejemplos.josecabrera.com.udacitycourse.model.ForecastDay;
import ejemplos.josecabrera.com.udacitycourse.utils.DateUtils;

public final class ForecastDayParser {

    private static final String OWM_PRESSURE = "pressure";
    private static final String OWM_HUMIDITY = "humidity";
    private static final String OWM_WINDSPEED = "speed";
    private static final String OWM_WIND_DIRECTION = "deg";

    private static final String OWM_TEMPERATURE = "temp";
    private static final String OWM_MAX = "max";
    private static final String OWM_MIN = "min";

    private static final String OWM_WEATHER = "weather";
    private static final String OWM_DESCRIPTION = "main";
    private static final String OWM_WEATHER_ID = "id";


    public static ForecastDay getForecastDayFromJson(JSONObject forecastDayJson, long dateTime)
            throws JSONException {

        ForecastDay forecastDay = new ForecastDay();
        forecastDay.setDateTime(dateTime);
        forecastDay.setPressure(forecastDayJson.getDouble(OWM_PRESSURE));
        forecastDay.setHumidity(forecastDayJson.getInt(OWM_HUMIDITY));
        forecastDay.setWindSpeed(forecastDayJson.getDouble(OWM_WINDSPEED));
        forecastDay.setWindDirection(forecastDayJson.getDouble(OWM_WIND_DIRECTION));

        JSONObject weatherObject = forecastDayJson.getJSONArray(OWM_WEATHER).getJSONObject(0);
        forecastDay.setDescription(weatherObject.getString(OWM_DESCRIPTION));
        forecastDay.setWeatherId(weatherObject.getInt(OWM_WEATHER_ID));

        JSONObject temperatureObject = forecastDayJson.getJSONObject(OWM_TEMPERATURE);
        forecastDay.setHighTemperature(temperatureObject.getDouble(OWM_MAX));
        forecastDay.setLowTemperature(temperatureObject.getDouble(OWM_MIN));

        return forecastDay;
    }

    public static List<ForecastDay> getForecastListFromJson(JSONArray forecastPerDay)
            throws JSONException {
        List<ForecastDay> forecast = new ArrayList<>();
        Calendar calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);


        for (int i = 0; i < forecastPerDay.length(); i++) {
            ForecastDay forecastDay = getForecastDayFromJson(forecastPerDay.getJSONObject(i), calendar.getTimeInMillis());
            forecast.add(forecastDay);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }

        return forecast;
    }
}
