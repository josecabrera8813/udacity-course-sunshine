package ejemplos.josecabrera.com.udacitycourse.model;


public class ForecastDay {

    private long dateTime;
    private double pressure;
    private int humidity;
    private double windSpeed;
    private double windDirection;
    private double highTemperature;
    private double lowTemperature;
    private String description;
    private int weatherId;

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public double getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(double windDirection) {
        this.windDirection = windDirection;
    }

    public double getHighTemperature() {
        return highTemperature;
    }

    public void setHighTemperature(double high) {
        this.highTemperature = high;
    }

    public double getLowTemperature() {
        return lowTemperature;
    }

    public void setLowTemperature(double low) {
        this.lowTemperature = low;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(int weatherId) {
        this.weatherId = weatherId;
    }

}
