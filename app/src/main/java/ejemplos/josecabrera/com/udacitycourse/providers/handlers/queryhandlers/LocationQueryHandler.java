package ejemplos.josecabrera.com.udacitycourse.providers.handlers.queryhandlers;


import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.providers.WeatherProvider;

public class LocationQueryHandler extends QueryHandler {

    public LocationQueryHandler( WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        super( weatherDbHelper, uriMatcher);
    }

    @Override
    public Cursor handleRequestQuery(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (mUriMatcher.match(uri) == WeatherProvider.LOCATION) {
            return mWeatherDbHelper.getReadableDatabase().query(
                    LocationContract.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder);

        } else {
            return mSuccessor.handleRequestQuery(uri, projection, selection, selectionArgs, sortOrder);
        }
    }

    @Override
    protected String buildSelection() {
        return null;
    }
}
