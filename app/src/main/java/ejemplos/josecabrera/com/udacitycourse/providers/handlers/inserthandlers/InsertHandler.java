package ejemplos.josecabrera.com.udacitycourse.providers.handlers.inserthandlers;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.net.Uri;

import ejemplos.josecabrera.com.udacitycourse.data.WeatherDbHelper;

public abstract class InsertHandler {
    protected InsertHandler mSuccessor;
    protected WeatherDbHelper mWeatherDbHelper;
    protected UriMatcher mUriMatcher;

    public InsertHandler(WeatherDbHelper weatherDbHelper, UriMatcher uriMatcher) {
        mWeatherDbHelper = weatherDbHelper;
        mUriMatcher = uriMatcher;
    }

    public void setSuccessor(InsertHandler successor) {
        mSuccessor = successor;
    }

    public abstract Uri handleRequestInsert(Uri uri, ContentValues values);
}
