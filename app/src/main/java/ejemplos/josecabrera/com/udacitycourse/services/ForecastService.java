

 package ejemplos.josecabrera.com.udacitycourse.services;

 import android.app.Service;
 import android.content.Intent;
 import android.os.IBinder;
 import android.support.annotation.Nullable;
 import android.util.Log;

 import ejemplos.josecabrera.com.udacitycourse.syncadapters.ForecastSyncAdapter;


 public class ForecastService extends Service {

     private static final Object sSyncAdapterLock = new Object();
     private static ForecastSyncAdapter sSunshineSyncAdapter = null;

     @Override
     public void onCreate() {
         Log.d("SunshineSyncService", "onCreate - SunshineSyncService");
         synchronized (sSyncAdapterLock) {
             if (sSunshineSyncAdapter == null) {
                 sSunshineSyncAdapter = new ForecastSyncAdapter(getApplicationContext(), true);
             }
         }
     }

     @Override
     public IBinder onBind(Intent intent) {
         return sSunshineSyncAdapter.getSyncAdapterBinder();
     }
 }