package ejemplos.josecabrera.com.udacitycourse.syncadapters;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import ejemplos.josecabrera.com.udacitycourse.R;
import ejemplos.josecabrera.com.udacitycourse.SunshineApplication;
import ejemplos.josecabrera.com.udacitycourse.activities.ForeCastActivity;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.LocationContract;
import ejemplos.josecabrera.com.udacitycourse.data.contracts.WeatherContract;
import ejemplos.josecabrera.com.udacitycourse.events.SyncFinishEvent;
import ejemplos.josecabrera.com.udacitycourse.model.ForecastDay;
import ejemplos.josecabrera.com.udacitycourse.model.LocationForecast;
import ejemplos.josecabrera.com.udacitycourse.parsers.LocationForecastParser;
import ejemplos.josecabrera.com.udacitycourse.utils.DateUtils;
import ejemplos.josecabrera.com.udacitycourse.utils.DrawableHelper;
import ejemplos.josecabrera.com.udacitycourse.utils.FormatHelper;
import ejemplos.josecabrera.com.udacitycourse.utils.SharedPreferencesUtil;

public class ForecastSyncAdapter extends AbstractThreadedSyncAdapter {

    private static final int ID_NOTIFICATION = 3004;
    private static final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
    private static final String QUERY_PARAM = "q";
    private static final String FORMAT_PARAM = "mode";
    private static final String UNITS_PARAM = "units";
    private static final String DAYS_PARAM = "cnt";
    private static final String APPID_PARAM = "APPID";
    private static final String FORMAT_VALUE = "json";
    private static final String UNITS_VALUE = "metric";
    private static final int DAYS_VALUE = 14;
    private static final String APPID_VALUE = "efbcc02875d91f5039e4c34af5fa2e82";

    public static final int SYNC_INTERVAL = 60 * 180;
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;
    private static final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;


    public static final String LOG_TAG = ForecastSyncAdapter.class.getSimpleName();

    private Listener<String> responseListener = new Listener<String>() {
        @Override
        public void onResponse(String response) {
            try {
                LocationForecast locationForecast = LocationForecastParser.getLocationForecastFromJson(response);
                saveLocationForecast(locationForecast);
            } catch (JSONException error) {
            }


        }
    };

    private ErrorListener responseErrorListener = new ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    public ForecastSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.d(LOG_TAG, "Init sync");
        String location = SharedPreferencesUtil.getPreferredLocation(getContext());
        Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                .appendQueryParameter(QUERY_PARAM, location)
                .appendQueryParameter(FORMAT_PARAM, FORMAT_VALUE)
                .appendQueryParameter(UNITS_PARAM, UNITS_VALUE)
                .appendQueryParameter(DAYS_PARAM, Integer.toString(DAYS_VALUE))
                .appendQueryParameter(APPID_PARAM, APPID_VALUE)
                .build();

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                builtUri.toString(),
                responseListener,
                responseErrorListener);
        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(stringRequest);

    }

    private void saveLocationForecast(LocationForecast locationForecast) {
        String locationSetting = SharedPreferencesUtil.getPreferredLocation(getContext());

        long locationId = addLocation(
                locationSetting,
                locationForecast.getCityName(),
                locationForecast.getCityLatitude(),
                locationForecast.getCityLongitude());

        List<ForecastDay> forecastDays = locationForecast.getForecast();
        ContentValues[] contentValuesList = new ContentValues[forecastDays.size()];


        for (int i = 0; i < contentValuesList.length; i++) {
            ContentValues weatherValues = new ContentValues();
            ForecastDay forecastDay = forecastDays.get(i);
            weatherValues.put(WeatherContract.COLUMN_LOC_KEY, locationId);
            weatherValues.put(WeatherContract.COLUMN_DATE, forecastDay.getDateTime());
            weatherValues.put(WeatherContract.COLUMN_HUMIDITY, forecastDay.getHumidity());
            weatherValues.put(WeatherContract.COLUMN_PRESSURE, forecastDay.getPressure());
            weatherValues.put(WeatherContract.COLUMN_WIND_SPEED, forecastDay.getWindSpeed());
            weatherValues.put(WeatherContract.COLUMN_DEGREES, forecastDay.getWindDirection());
            weatherValues.put(WeatherContract.COLUMN_MAX_TEMP, forecastDay.getHighTemperature());
            weatherValues.put(WeatherContract.COLUMN_MIN_TEMP, forecastDay.getLowTemperature());
            weatherValues.put(WeatherContract.COLUMN_SHORT_DESC, forecastDay.getDescription());
            weatherValues.put(WeatherContract.COLUMN_WEATHER_ID, forecastDay.getWeatherId());
            contentValuesList[i] = weatherValues;
        }

        getContext().getContentResolver().bulkInsert(WeatherContract.CONTENT_URI, contentValuesList);

        Calendar calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(DateUtils.normalizeDate(System.currentTimeMillis()));
        calendar.add(Calendar.DAY_OF_YEAR,-1);

        getContext().getContentResolver().delete(
                WeatherContract.CONTENT_URI,
                WeatherContract.COLUMN_DATE + " <= ?",
                new String[]{Long.toString(calendar.getTimeInMillis())});

        SunshineApplication.getBus().post(new SyncFinishEvent(true));
        notifyWeather();
        Log.d(LOG_TAG, "Inserted " + contentValuesList.length + " rows");


    }


    private long addLocation(String locationSetting, String cityName, double lat, double lon) {
        long locationId;

        Cursor locationCursor = getContext().getContentResolver().query(
                LocationContract.CONTENT_URI,
                new String[]{LocationContract._ID},
                LocationContract.COLUMN_LOCATION_SETTING + " = ?",
                new String[]{locationSetting},
                null);

        if (locationCursor.moveToFirst()) {
            int locationIdIndex = locationCursor.getColumnIndex(LocationContract._ID);
            locationId = locationCursor.getLong(locationIdIndex);
        } else {
            ContentValues locationValues = new ContentValues();

            locationValues.put(LocationContract.COLUMN_CITY_NAME, cityName);
            locationValues.put(LocationContract.COLUMN_LOCATION_SETTING, locationSetting);
            locationValues.put(LocationContract.COLUMN_COORD_LAT, lat);
            locationValues.put(LocationContract.COLUMN_COORD_LONG, lon);

            Uri insertedUri = getContext().getContentResolver().insert(
                    LocationContract.CONTENT_URI,
                    locationValues
            );

            locationId = ContentUris.parseId(insertedUri);
        }

        locationCursor.close();
        return locationId;
    }

    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(), syncInterval);
        }
    }


    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context),
                context.getString(R.string.content_authority), bundle);
    }


    public static Account getSyncAccount(Context context) {
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        if (null == accountManager.getPassword(newAccount)) {

            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {
        ForecastSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);
        syncImmediately(context);
    }

    private void notifyWeather() {
        Context context = getContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String lastNotificationKey = context.getString(R.string.pref_last_notification);
        long lastSync = prefs.getLong(lastNotificationKey, 0);

        if (System.currentTimeMillis() - lastSync >= DAY_IN_MILLIS) {
            String locationQuery = SharedPreferencesUtil.getPreferredLocation(context);

            Uri weatherUri = WeatherContract.buildWeatherLocationWithDate(locationQuery, System.currentTimeMillis());

            Cursor cursor = context.getContentResolver().query(weatherUri, null, null, null, null);

            if (cursor.moveToFirst()) {
                int weatherId = cursor.getInt(cursor.getColumnIndex(WeatherContract.COLUMN_WEATHER_ID));
                double high = cursor.getDouble(cursor.getColumnIndex(WeatherContract.COLUMN_MAX_TEMP));
                double low = cursor.getDouble(cursor.getColumnIndex(WeatherContract.COLUMN_MIN_TEMP));
                String desc = cursor.getString(cursor.getColumnIndex(WeatherContract.COLUMN_SHORT_DESC));

                int iconId = DrawableHelper.getIconResourceForWeatherCondition(weatherId);
                String title = context.getString(R.string.app_name);

                String contentText = String.format(
                        context.getString(R.string.format_notification),
                        desc,
                        FormatHelper.formatTemperature(context, high, SharedPreferencesUtil.isMetric(context)),
                        FormatHelper.formatTemperature(context, low, SharedPreferencesUtil.isMetric(context)));

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(context)
                                .setSmallIcon(iconId)
                                .setContentTitle(title)
                                .setContentText(contentText);

                Intent resultIntent = new Intent(context, ForeCastActivity.class);

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                stackBuilder.addParentStack(ForeCastActivity.class);
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                mBuilder.setContentIntent(resultPendingIntent);

                NotificationManager mNotificationManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.notify(ID_NOTIFICATION, mBuilder.build());


                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(lastNotificationKey, System.currentTimeMillis());
                editor.commit();
            }
        }

    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }
}
